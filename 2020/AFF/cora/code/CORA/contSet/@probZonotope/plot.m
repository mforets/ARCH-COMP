function plot(probZ,varargin)
% plot - Plots 2-dimensional projection of a zonotope with a maximum
%    of 5 generators
%
% Syntax:  
%    plot(probZ,dimensions)
%
% Inputs:
%    probZ - probabilistic zonotope object
%    type - (optional) plot type: available options
%               'solid' (default)
%               'mesh'
%               'meshHide'
%               'blue'
%               'green'
%               'dark'
%               'light' 
%    dims - (optional) dimensions that should be projected, default: [1,2]
%    m - (optional) m-sigma value, default: probZ.gamma
%
% Outputs:
%    ---
%
% Example:
%    Z1 = [10 1 -2; 0 1 1];
%    Z2 = [0.6 1.2; 0.6 -1.2];
%    probZ = probZonotope(Z1,Z2);
%    plot(pZ,'dark');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Matthias Althoff
% Written:       03-August-2007
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------

%If only one argument is passed
if nargin==1
    type='solid';
    dims=[1,2];
    m=probZ.gamma;
    
%If two arguments are passed    
elseif nargin==2
    type=varargin{1};
    dims=[1,2];
    m=probZ.gamma;
    
%If three arguments are passed    
elseif nargin==3
    type=varargin{1};
    dims=varargin{2};
    m=probZ.gamma;
    
%If four arguments are passed    
elseif nargin==4
    type=varargin{1};
    dims=varargin{2};
    m=varargin{3};     
    
%If too many arguments are passed
else
    disp('Error: too many inputs');
    dims=varargin{1};
end
    

%compute enclosing probability
eP = enclosingProbability(probZ,m,dims);

%open new figure
%figure
%colormap('cool')
%colormap('hot')

% l=linspace(1,0,100)';
% o=ones(100,1);
% colormap([l,l,o]);

%colormap([0,0,0]);


%plot graph
if strcmp(type,'mesh')
    mesh(eP.X,eP.Y,eP.P);
    hidden off
elseif strcmp(type,'meshHide')
    colormap([0,0,0]);
    mesh(eP.X,eP.Y,eP.P); 
elseif strcmp(type,'blue')
    colormap([0,0,1]);
    surf(eP.X,eP.Y,eP.P,'FaceColor','b',...
    'EdgeColor','none',...
    'FaceLighting','phong')
    material dull
    %alpha(.4)
    %set lights
    %camlight right
    %camlight left
    camlight headlight   
elseif strcmp(type,'green')
    surf(eP.X,eP.Y,eP.P,'FaceColor','g',...
    'EdgeColor','none',...
    'FaceLighting','phong')
    material dull
    %alpha(.4)
    %set lights
    %camlight right
    %camlight left
    camlight headlight    
elseif strcmp(type,'dark')
    surf(eP.X,eP.Y,eP.P,'FaceColor',[0.2 0.2 0.2],...
    'EdgeColor','none',...
    'FaceLighting','phong')
    material dull
    %alpha(.4)
    %set lights
    %camlight right
    %camlight left
    camlight headlight   
elseif strcmp(type,'light')
    surf(eP.X,eP.Y,eP.P,'FaceColor',[0.5 0.5 0.5],...
    'EdgeColor','none',...
    'FaceLighting','phong')
    material dull
    %alpha(.4)
    %set lights
    %camlight right
    %camlight left
    camlight headlight        
else
    l=linspace(1,0,100)';
    o=ones(100,1);
    colormap([l,l,l]);

    cmap = colormap;
    newCmap=[ones(1,3);cmap(2:end,:)];
    %newCmap=cmap;
    colormap(newCmap);
    
    surf(eP.X,eP.Y,eP.P,'FaceColor','interp',...
    'EdgeColor','none')    
    
%     surf(eP.X,eP.Y,eP.P,'FaceColor','interp',...
%     'EdgeColor','none',...
%     'FaceLighting','phong')
%     material dull 
end



%daspect([40 40 1])
%axis([-2 1.8 -2 1 0 1.3])
% xlim([-2 1.8])
% ylim([-2 1])
% zlim([0 1.3])

%------------- END OF CODE --------------