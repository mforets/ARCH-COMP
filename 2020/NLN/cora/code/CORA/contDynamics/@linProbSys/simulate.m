function [t,x,ind] = simulate(obj,params,options)
% simulate - simulates the linear interval system within a location
%
% Syntax:  
%    [t,x,ind] = simulate(obj,params,options)
%
% Inputs:
%    obj - linProbSys object
%    params - struct containing the parameters for the simulation
%       .tStart initial time t0
%       .tFinal final time tf
%       .x0 initial point x0
%       .u piecewise constant input signal u(t) specified as a matrix
%           for which the number of rows is identical to the number of
%           system input
%       .p parameter value p (class nonlinParamSys only)
%       .startLoc index of the initial location (class hybridAutomaton and
%           parallelHybridAutomaton only)
%       .finalLoc index of the final location (class hybridAutomaton and
%           parallelHybridAutomaton only)
%    options - ODE45 options (for hybrid systems)
%
% Outputs:
%    t - time vector
%    x - state vector
%    ind - returns the event which has been detected
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      16-May-2007 
% Last update:  26-February-2008
% Last revision:---

%------------- BEGIN CODE --------------

%self-programmed euler solver
h=params.timeStep/5;
t(1)=params.tStart;
x(:,1)=params.x0;

%obtain dimension
dim=length(obj.A);

for i=1:(ceil((params.tFinal-params.tStart)/h)+1) %+1 to enforce that simulation is quit
    
    %compute random value from the noise signal
    mu=zeros(dim,1);
    Sigma=1/h*eye(dim);
    u=obj.C*mvnrnd(mu,Sigma)';
    
    %next state
    x(:,i+1)=expm(obj.A*h)*x(:,i)+... %initial solution
        inv(obj.A)*(expm(obj.A*h)-eye(length(obj.A)))*(params.u+u); %input solution
end
index=[];
x=x';
%ode4: fixed step size Runge-Kutta

    
%------------- END OF CODE --------------