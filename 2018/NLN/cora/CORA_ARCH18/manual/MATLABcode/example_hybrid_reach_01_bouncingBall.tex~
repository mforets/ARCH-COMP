% This file was automatically created from the m-file 
% "m2tex.m" written by USL. 
% The fontencoding in this file is UTF-8. 
%  
% You will need to include the following two packages in 
% your LaTeX-Main-File. 
%  
% \usepackage{color} 
% \usepackage{fancyvrb} 
%  
% It is advised to use the following option for Inputenc 
% \usepackage[utf8]{inputenc} 
%  
  
% definition of matlab colors: 
\definecolor{mblue}{rgb}{0,0,1} 
\definecolor{mgreen}{rgb}{0.13333,0.5451,0.13333} 
\definecolor{mred}{rgb}{0.62745,0.12549,0.94118} 
\definecolor{mgrey}{rgb}{0.5,0.5,0.5} 
\definecolor{mdarkgrey}{rgb}{0.25,0.25,0.25} 
  
\DefineShortVerb[fontfamily=courier,fontseries=m]{\$} 
\DefineShortVerb[fontfamily=courier,fontseries=b]{\#} 
  
\noindent                                                                                               
 \hspace*{-1.6em}{\scriptsize 1}$  $\color{mblue}$function$\color{black}$ example_hybrid_reach_01_bouncingBall$\\
 \hspace*{-2em}{\scriptsize 28}$  $\\
 \hspace*{-2em}{\scriptsize 29}$  $\color{mgreen}$%set options---------------------------------------------------------------$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 30}$  options.x0 = [1; 0]; $\color{mgreen}$%initial state$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 31}$  options.R0 = zonotope([options.x0, diag([0.05, 0.05])]); $\color{mgreen}$%initial set$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 32}$  options.startLoc = 1; $\color{mgreen}$%initial location$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 33}$  options.finalLoc = 0; $\color{mgreen}$%0: no final location$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 34}$  options.tStart = 0; $\color{mgreen}$%start time$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 35}$  options.tFinal = 1.7; $\color{mgreen}$%final time$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 36}$  options.timeStepLoc{1} = 0.05; $\color{mgreen}$%time step size$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 37}$  options.taylorTerms = 10;$\\
 \hspace*{-2em}{\scriptsize 38}$  options.zonotopeOrder = 20;$\\
 \hspace*{-2em}{\scriptsize 39}$  options.polytopeOrder = 10;$\\
 \hspace*{-2em}{\scriptsize 40}$  options.errorOrder=2;$\\
 \hspace*{-2em}{\scriptsize 41}$  options.reductionTechnique = $\color{mred}$'girard'$\color{black}$;$\\
 \hspace*{-2em}{\scriptsize 42}$  options.isHyperplaneMap = 0;$\\
 \hspace*{-2em}{\scriptsize 43}$  options.enclosureEnables = 5; $\color{mgreen}$%choose enclosure method(s)$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 44}$  options.originContained = 0;$\\
 \hspace*{-2em}{\scriptsize 45}$  $\color{mgreen}$%--------------------------------------------------------------------------$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 46}$  $\\
 \hspace*{-2em}{\scriptsize 47}$  $\\
 \hspace*{-2em}{\scriptsize 48}$  $\color{mgreen}$%specify hybrid automaton--------------------------------------------------$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 49}$  $\color{mgreen}$%specify linear system of bouncing ball$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 50}$  A = [0 1; 0 0];$\\
 \hspace*{-2em}{\scriptsize 51}$  B = eye(2); $\color{mgreen}$% no loss of generality to specify B as the identity matrix$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 52}$  linSys = linearSys($\color{mred}$'linearSys'$\color{black}$,A,B);$\\
 \hspace*{-2em}{\scriptsize 53}$  $\\
 \hspace*{-2em}{\scriptsize 54}$  $\color{mgreen}$%define large and small distance$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 55}$  dist = 1e3;$\\
 \hspace*{-2em}{\scriptsize 56}$  eps = 1e-6;$\\
 \hspace*{-2em}{\scriptsize 57}$  alpha = -0.75; $\color{mgreen}$%rebound factor$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 58}$  $\\
 \hspace*{-2em}{\scriptsize 59}$  $\color{mgreen}$%invariant$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 60}$  inv = interval([-2*eps; -dist], [dist; dist]);$\\
 \hspace*{-2em}{\scriptsize 61}$  $\color{mgreen}$%guard sets$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 62}$  guard = interval([-eps; -dist], [0; -eps]); $\\
 \hspace*{-2em}{\scriptsize 63}$  $\color{mgreen}$%resets$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 64}$  reset.A = [0, 0; 0, alpha]; reset.b = zeros(2,1);$\\
 \hspace*{-2em}{\scriptsize 65}$  $\color{mgreen}$%transitions$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 66}$  trans{1} = transition(guard,reset,1,$\color{mred}$'a'$\color{black}$,$\color{mred}$'b'$\color{black}$); $\color{mgreen}$%--> next loc: 1; $\color{mred}$'a'$\color{black}$, $\color{mred}$'b'$\color{black}$ are dummies$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 67}$  $\color{mgreen}$%specify location$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 68}$  loc{1} = location($\color{mred}$'loc1'$\color{black}$,1,inv,trans,linSys); $\\
 \hspace*{-2em}{\scriptsize 69}$  $\color{mgreen}$%specify hybrid automata$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 70}$  HA = hybridAutomaton(loc); $\color{mgreen}$% for "geometric intersection"$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 71}$  $\color{mgreen}$%--------------------------------------------------------------------------$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 72}$  $\\
 \hspace*{-2em}{\scriptsize 73}$  $\color{mgreen}$%set input:$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 74}$  options.uLoc{1} = [0; -9.81]; $\color{mgreen}$%input for simulation$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 75}$  options.uLocTrans{1} = options.uLoc{1}; $\color{mgreen}$%input center$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 76}$  options.Uloc{1} = zonotope(zeros(2,1)); $\color{mgreen}$%input deviation $\color{black}$$\\
 \hspace*{-2em}{\scriptsize 77}$  $\\
 \hspace*{-2em}{\scriptsize 78}$  $\color{mgreen}$%simulate hybrid automaton$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 79}$  HA = simulate(HA,options); $\\
 \hspace*{-2em}{\scriptsize 80}$  $\\
 \hspace*{-2em}{\scriptsize 81}$  $\color{mgreen}$%compute reachable set$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 82}$  [HA] = reach(HA,options);$\\
 \hspace*{-2em}{\scriptsize 83}$  $\\
 \hspace*{-2em}{\scriptsize 84}$  $\color{mgreen}$%choose projection and plot------------------------------------------------$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 85}$  figure $\\
 \hspace*{-2em}{\scriptsize 86}$  hold $\color{mred}$on$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 87}$  options.projectedDimensions = [1 2];$\\
 \hspace*{-2em}{\scriptsize 88}$  options.plotType = $\color{mred}$'b'$\color{black}$;$\\
 \hspace*{-2em}{\scriptsize 89}$  plot(HA,$\color{mred}$'reachableSet'$\color{black}$,options); $\color{mgreen}$%plot reachable set$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 90}$  plotFilled(options.R0,options.projectedDimensions,$\color{mred}$'w'$\color{black}$,$\color{mred}$'EdgeColor'$\color{black}$,$\color{mred}$'k'$\color{black}$); $\color{mgreen}$$\\
 \hspace*{-2em}{\scriptsize 91}$  plot(HA,$\color{mred}$'simulation'$\color{black}$,options); $\color{mgreen}$%plot simulation$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 92}$  axis([0,1.2,-6,4]);$\\
 \hspace*{-2em}{\scriptsize 93}$  $\color{mgreen}$%--------------------------------------------------------------------------$\color{black}$$\\
  
\UndefineShortVerb{\$} 
\UndefineShortVerb{\#}