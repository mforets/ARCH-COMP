% (C) 2019 National Institute of Advanced Industrial Science and Technology
% (AIST)

% SteamCondenser Benchmark
%%%%%%%%%%%%%%%%%%%%

tmpl = struct();
tmpl.mdl = 'steamcondense_RNN_22';
tmpl.input_range = [3.99 4.01];
tmpl.output_range = [70 80; 100 125; 8800 9500 ; 86 90];
tmpl.init_opts = {};
tmpl.gen_opts = {};
tmpl.interpolation = {'pconst'};
tmpl.option = {};
tmpl.maxEpisodes = maxEpisodes;
tmpl.agentName = 'Falsifier';
tmpl.stopTime = 35;

%Formula 1
fml1 = struct(tmpl);
fml1.expName = 'fml1';
fml1.targetFormula = '[]_[30,35](pL /\ pH)';
fml1.monitoringFormula = 'pL /\ pH';
fml1.preds(1).str = 'pL';
fml1.preds(1).A = [0 0 0 -1];
fml1.preds(1).b = -87;
fml1.preds(2).str = 'pH';
fml1.preds(2).A = [0 0 0 1];
fml1.preds(2).b = 87.5;

formulas = {fml1};

configs = { };
for k = 1:size(formulas, 2)
    for i = 1:size(algorithms, 2)
        config = struct(formulas{k});
        config.sampleTime = 0.1;
        config.algoName = algorithms{i};
        for l = 1:maxIter
            configs = [configs, config];
        end
    end
end

for k = 1:size(formulas, 2)
    for i = 1:size(algorithms, 2)
        config = struct(formulas{k});
        config.sampleTime = 1.75;
        config.algoName = algorithms{i};
        for l = 1:maxIter
            configs = [configs, config];
        end
    end
end

do_experiment('SteamCondenser', configs);
