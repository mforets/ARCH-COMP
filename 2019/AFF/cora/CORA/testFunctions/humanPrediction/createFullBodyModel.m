function [v_max_Markers, a_max_Markers] = createFullBodyModel()
% createFullBodyModel - returns parameters v_max and a_max for each marker
% on the body so that the data of all markers is reachset confrmant with 
% the obtained model.
% 
%
% Syntax:  
%    [v_max, a_max] = createFullBodyModel()
%
% Inputs:
%
% Outputs:
%    v_max - maximum velocity
%    a_max - maximum acceleration
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      05-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% load data
load CMU_data_for_Science_corrected_10Mar2019c_movingWindow20_meanFilter10


% set measurement uncertainties
factor = 1000; % mm to m ?
% s_0_delta = factor*0.002; %[m]
% v_0_delta = factor*0.02; %[m/s]

s_0_delta = factor*0.04; %[m]
v_0_delta = factor*0.4; %[m/s]

% s_0_delta = factor*0.04; %[m]
% v_0_delta = factor*0.04; %[m/s]

% set increments for v and a
delta_v = factor*0.1;
delta_a = factor*0.1;

% initial values
v_start = factor*1;
a_start = factor*1;

% number of movements
nrOfMovements = length(ALLMOVEMENTS(:,1))-1;

%for iMovement = 1:nrOfMovements
for iMovement = 32:32
%for iMovement = 114:114
    profile on
    % extract trajectories of all markers
    fullTraj = ALLMOVEMENTS{iMovement+1,2};
    
    % frame rate
    frameRate = ALLMOVEMENTS{iMovement+1,3};
    
    % time increment
    delta_t = 1/frameRate;

    % time steps
    timeSteps = length(fullTraj(:,1));

    % time vector
    t = linspace(0,timeSteps*delta_t,timeSteps);
    
    % number of markers
    nrOfMarkers = length(fullTraj(1,:))/3;

    for iMarker = 1:nrOfMarkers
        % extract trajectory
        selectedIndices = 3*(iMarker-1)+1 : 3*(iMarker-1)+3;
        traj = fullTraj(:,selectedIndices);
        % synthesize parameters
        [v_max, a_max] = conformantParameters(traj, s_0_delta, v_0_delta, t, delta_v, delta_a, v_start, a_start);
        % store result
        v_max_Markers(iMovement,iMarker) = v_max;
        a_max_Markers(iMovement,iMarker) = a_max;

        iMovement
        iMarker
    end
    profile off
    profile viewer
    save fullBodyModel_test v_max_Markers a_max_Markers
end

% %% for CMU data:
% % running examples (high acceleration on impact)
% runningExamples = 111:120;
% 
% %exclude running examples
% a_max_noRunning = a_max_Markers;
% a_max_noRunning(runningExamples,:) = [];
% 
% v_max_noRunning = v_max_Markers;
% v_max_noRunning(runningExamples,:) = [];
% 
% % v_max 
% max(v_max_Markers);
% 
% % a_max
% max(a_max_Markers);
% 
% % v_max; no running
% max(v_max_noRunning);
% 
% % a_max; no running
% max(a_max_noRunning);




%------------- END OF CODE --------------