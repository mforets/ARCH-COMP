function [val,x] = supportFunc(E,dir,varargin)
% supportFunc - Calculate the upper or lower bound of an ellipsoid object
%               along a certain direction (see Def. 2.1.2 in [1]) 
%
% Syntax:  
%    [val,x] = supportFunc(E,dir)
%    [val,x] = supportFunc(E,dir,type)
%
% Inputs:
%    E   - ellipsoid object
%    dir - direction for which the bounds are calculated (vector of size
%          (n,1) )
%    type - upper or lower bound ('lower' or 'upper')
%
% Outputs:
%    val - bound of the ellipsoid in the specified direction
%    x   - point for which holds: dir'*x=val
%
% Example: 
%    E = ellipsoid([5 7;7 13],[1;2]);
%
%    val = supportFunc(E,[1;1]);
%   
%    figure
%    hold on
%    plot(E,[1,2],'b');
%    plot(conHyperplane(halfspace([1;1],val),[],[]),[1,2],'g');
%
% References: 
%   [1] A. Kurzhanski et al. "Ellipsoidal Toolbox Manual", 2006
%       https://www2.eecs.berkeley.edu/Pubs/TechRpts/2006/EECS-2006-46.pdf
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/supportFunc

% Author:       Victor Gassmann
% Written:      20-November-2019
% Last update:  12-March-2021
% Last revision:---

%------------- BEGIN CODE --------------

    % parse input arguments
    type = 'upper';
    
    if nargin >= 3 && ~isempty(varargin{1})
        type = varargin{1};
    end

    if strcmp(type,'upper')
        s = 1;
    else
        s = -1;
        
    end
    val = dir'*E.q + s*sqrt(dir'*E.Q*dir);
    if nargout > 1
        % find point x which attains the value by forming hyperplane and
        % intersecting with ellipsoid
        H = conHyperplane(dir',val);
        E_res = E&H;
        assert(~isempty(E_res),'intersection with tangent hp should not be empty');
        % basically contains only 1 point
        x = E_res.q;
    end
end
%------------- END OF CODE --------------