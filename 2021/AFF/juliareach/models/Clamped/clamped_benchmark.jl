using BenchmarkTools, Plots, Plots.PlotMeasures, LaTeXStrings
using BenchmarkTools: minimum, median

SUITE = BenchmarkGroup()
model = "CLAMPED"
cases = ["CB21d-100-C", # constant input set
         "CB21d-100-F"] # bounded but arbitrarily-varying input set

SUITE[model] = BenchmarkGroup()

include("clamped.jl")

# ----------------------------------------
#  CB21d-100-C
# ----------------------------------------
S = clamped(N=100, a=1e-6, b=1e-6, damped=true, homogeneize=true)
alg = LGG09(δ=1e-6, vars=[70, 170], n=201)
ΔF0 = Interval(0.99, 1.01)
X0 = Singleton(zeros(200)) × ΔF0
prob = InitialValueProblem(S, X0)

sol_CB21d100C = solve(prob, NSTEPS=10_000, alg=alg)  # warm-up run
property = 1 # nothing to verify
push!(validation, Int(property))

SUITE[model][cases[1]] = @benchmarkable solve($prob, NSTEPS=10_000, alg=$alg)

# ----------------------------------------
#  CB21d-100-F
# ----------------------------------------
S = clamped(N=100, a=1e-6, b=1e-6, damped=true, homogeneize=false, constant=false)
alg = LGG09(δ=1e-7, vars=[70, 170], n=200)
X0 = Singleton(zeros(200))
prob = InitialValueProblem(S, X0)

sol_CB21d100F = solve(prob, NSTEPS=100_000, alg=alg) # warm-up run
property = 1 # nothing to verify
push!(validation, Int(property))

SUITE[model][cases[2]] = @benchmarkable solve($prob, NSTEPS=10_000, alg=$alg)

# ==============================================================================
# Execute benchmarks and save benchmark results
# ==============================================================================

# tune parameters
tune!(SUITE)

# run the benchmarks
results = run(SUITE, verbose=true)

# return the sample with the smallest time value in each test
println("minimum time for each benchmark:\n", minimum(results))

# return the median for each test
println("median time for each benchmark:\n", median(results))

# export runtimes
runtimes = Dict()
for (i, c) in enumerate(cases)
   t = median(results[model][c]).time * 1e-9
   runtimes[c] = round(t, digits=4)
end

for (i, c) in enumerate(cases)
   print(io, "JuliaReach, $model, $c, $(validation[i]), $(runtimes[c])\n")
end

# ==============================================================================
# Plot
# ==============================================================================

# Constant force case
fig = plot()
plot!(fig, sol_CB21d100C, vars=(0, 70), lw=0.0, c=:blue, xlab="t", ylab="Position at node 70")
savefig(fig, joinpath(@__DIR__, "ARCH-COMP21-JuliaReach-Clamped_CB21d100C-pos.pdf"))

fig = plot()
plot!(fig, sol_CB21d100C, vars=(0, 170), lw=0.0, c=:blue, xlab="t", ylab="Velocity at node 70")
savefig(fig, joinpath(@__DIR__, "ARCH-COMP21-JuliaReach-Clamped_CB21d100C-vel.pdf"))

fig = plot(xlab="t", ylab="Velocity at node 70", xlims=(2e-4, 1e-3), ylims=(60, 75))
plot!(fig, sol_CB21d100C, vars=(0, 170), lw=0.0, c=:blue)
savefig(fig, joinpath(@__DIR__, "ARCH-COMP21-JuliaReach-Clamped_CB21d100C-vel-zoom.pdf"))

# Time-varying force case
fig = plot()
plot!(fig, sol_CB21d100F, vars=(0, 70), lw=0.0, c=:blue, xlab="t", ylab="Position at node 70")
savefig(fig, joinpath(@__DIR__, "ARCH-COMP21-JuliaReach-Clamped_CB21d100F-pos.pdf"))

fig = plot()
plot!(fig, sol_CB21d100F, vars=(0, 170), lw=0.0, c=:blue, xlab="t", ylab="Velocity at node 70")
savefig(fig, joinpath(@__DIR__, "ARCH-COMP21-JuliaReach-Clamped_CB21d100F-vel.pdf"))

fig = plot(xlab="t", ylab="Velocity at node 70", xlims=(7e-3, 10e-3), ylims=(0, 80))
plot!(fig, sol_CB21d100F, vars=(0, 170), lw=0.0, c=:blue)
savefig(fig, joinpath(@__DIR__, "ARCH-COMP21-JuliaReach-Clamped_CB21d100F-vel-zoom.pdf"))

sol_CB21d100C = nothing
sol_CB21d100F = nothing
GC.gc()
