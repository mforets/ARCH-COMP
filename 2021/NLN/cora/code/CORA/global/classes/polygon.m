classdef polygon
% polygon class 
%
% Syntax:  
%    obj = polygon(x,y)
%
% Inputs:
%    x - vector with x coordinates of the polygon vertices
%    y - vector with y coordinates of the polygon vertices
%
% Outputs:
%    obj - polygon object
%
% Example:
%    x = gallery('uniformdata',30,1,1);
%    y = gallery('uniformdata',30,1,10);
%    ind = boundary(x,y);
%
%    pgon = polygon(x(ind),y(ind));
%
%    plot(pgon,[1,2],'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: interval, polytope

% Author:       Niklas Kochdumper
% Written:      13-March-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

properties (SetAccess = private, GetAccess = public)
    set = []
end
    
methods
    
    % class constructor
    function obj = polygon(x,y)
        obj.set = polyshape(x,y);
    end
    
    function han = plot(obj,dims,varargin)
    % plot the polygon object (always 2D!)
    % input argument dims is here for compliance with other plot functions)
    
        % parse input arguments
        filled = 0;
        linespec = 'b';
        NVpairs = {};
        
        if nargin >= 3
            [linespec,NVpairs] = readPlotOptions(varargin(1:end));
            [NVpairs,filled] = readNameValuePair(NVpairs,'Filled', ...
                                                 'islogical',filled);
        end
        
        % plot the polygon object
        if filled
            han = plot(obj.set,'FaceColor',linespec,'FaceAlpha',...
                       1,NVpairs{:});
        else
            han = plot(obj.set,'FaceColor','none','EdgeColor', ...
                       linespec,NVpairs{:});
        end
    end
    
    function c = center(obj)
    % get the center of the polygon
        [x,y] = centroid(obj.set);
        c = [x;y];
    end
    
    function res = isIntersecting(obj1,obj2)
    % check if two polygon object insterst
        temp = intersect(obj1.set,obj2.set);
        res = ~isempty(temp.Vertices);
    end
    
    function res = in(obj1,obj2)
    % check if polygon obj2 is inside polygon obj2
    
        if isnumeric(obj2)
            
            res = isinterior(obj1.set,obj2(1),obj2(2));
            
        elseif isa(obj2,'polygon')
            
            % compute union
            u = union(obj1.set,obj2.set);

            % check if area of obj1 is identical to area of union
            A1 = area(obj1.set);
            A2 = area(u);

            res = A1 == A2;
        else
            error('This set representation is not supported!');
        end
    end
    
    function obj = and(obj1,obj2)
    % computes the intersection of two polygons
    
        temp = intersect(obj1.set,obj2.set);
        V = temp.Vertices;
        obj = polygon(V(:,1),V(:,2));  
    end
    
    function obj = or(obj1,obj2)
    % computes the union of two polygons
        
        if isempty(obj1)
            obj = obj2; 
        elseif isempty(obj2)
            obj = obj1;
        else
            temp = union(obj1.set,obj2.set);
            V = temp.Vertices;
            obj = polygon(V(:,1),V(:,2));
        end
        
    end
    
    function obj = plus(obj,summand)
    % compute the minkowski sum with a point    
        
       % get polygon object
       if ~isa(obj,'polygon')
          temp = obj;
          obj = summand;
          summand = temp;
       end
       
       % different types of sets
       if isnumeric(summand)
           % translate the polygon
           obj.set = translate(obj.set,summand');
       elseif isa(summand,'polygon')
           % compute Minkowski sum
           w = warning();
           warning('off');
           
           T1 = triangulation(obj);
           T2 = triangulation(summand);
           obj = [];
           
           for i = 1:length(T1)
               for j = 1:length(T2)
                   V1 = T1{i}.set.Vertices;
                   V2 = T2{j}.set.Vertices;
                   V = [];
                   for k = 1:size(V1,1)
                       V = [V; V2 + V1(k,:)];
                   end
                   obj = obj | convHull(polygon(V(:,1),V(:,2)));
               end
           end
           
           warning(w);
       else
          error('Operation "plus" is not yet implemented for this set representation!'); 
       end
    end
    
    function obj = mtimes(mat,obj)
    % compute linear transformation of a polygon
       
       % check dimension of the matrix
       if ~isnumeric(mat) || size(mat,1) ~= 2 || size(mat,2) ~= 2
          error('Operation "mtimes" is only defined for square matrices of dimension 2!'); 
       end
       
       % multiplication with matrix
       w = warning();
       warning('off');
       
       V = obj.set.Vertices;
       V = (mat*V')'; 

       obj = polygon(V(:,1),V(:,2));
       
       warning(w);
    end
    
    function obj = convHull(obj1,varargin)
    % compute convex hull of a polygon
    
        % compute union if two sets are passed
        if nargin > 1
            temp = obj1 | varargin{1};
        else
            temp = obj1; 
        end
        
        % compute convex hull
        temp = convhull(temp.set);
        
        % construct resulting polygon object
        V = temp.Vertices;
        obj = polygon(V(:,1),V(:,2));
    end
    
    function int = interval(obj)
    % compute an interval enclosure of the polygon
    
        V = obj.set.Vertices;
        int = interval(min(V,[],1)',max(V,[],1)');
    end
    
    function list = triangulation(obj)
    % compute an triangulation of the polygon
    
        % compute triangulation
        T = triangulation(obj.set);
        
        % convert the triangles to polygons
        list = cell(size(T.ConnectivityList,1),1);
        
        for i = 1:size(T.ConnectivityList,1)
           V = T.Points(T.ConnectivityList(i,:),:);
           list{i} = polygon(V(:,1),V(:,2));
        end
    end
    
    function obj = simplify(obj,varargin)
    % enclose the polygon by a simpler polygon with less vertices   
        
        % parse input arguments
        tol = 0.01;
        if nargin >= 2 && ~isempty(varargin{1})
            tol = varargin{1};
        end
    
        % simplify polygon boundary using the Douglar-Peucker algorithm 
        V = obj.set.Vertices;
        V_ = douglasPeucker(V',tol);
        obj = polygon(V_(1,:),V_(2,:));
        
        % enlare the simplified polygon so that it is guaranteed that it
        % encloses the original polygon
        temp = polygon([-tol -tol tol tol],[-tol tol tol -tol]);
        obj = obj + temp;
    end
end
end

%------------- END OF CODE --------------