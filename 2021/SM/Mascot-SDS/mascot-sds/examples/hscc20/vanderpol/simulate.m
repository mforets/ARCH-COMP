%
% simulate.m
%
% created on: 09.10.2015
%     author: rungger (vehicle.m)
%     modified by: kaushik
%
%
% you need to run ./osc binary first 
%
% so that the file: osc_controller_under.bdd is created
%

function simulate
    clear set
    close all
    addpath(genpath('../../../mfiles'))

    %% simulation
    % initial state
    x0=[-3,4];
    
    % parameters for the simulation
    [~,~,W_ub,xlb,xub,tau] = readParams('input.hh');

    % prepare the figure window
    openfig('osc_domain')
    hold on
    xlim=[xlb(1) xub(1) xlb(2) xub(2)];
    axis(xlim);
    xticks([-5:1:5]);
    xticklabels({'-5','-4','-3','-2','-1','0','1','2','3','4','5'});
    yticks([-5:1:5]);
    yticklabels({'-5','-4','-3','-2','-1','0','1','2','3','4','5'});
    % label the initial state 
    text(-3.3,3.8,'$I$','interpreter','latex','FontSize',16);
    % label the target
    text(-1.5,-2,'$B$','interpreter','latex','FontSize',16);
    % draw a boundary around the target
    rectangle('Position',[-1.2,-2.9,0.3,0.9],'FaceColor',[51, 204, 51]/255);

    % load the symbolic set containing the controller
    controller=SymbolicSet('osc_controller_under.bdd');
    try
        controller.getInputs(x0);
    catch
        error('osc_plot:simulate_uniform_noise:initial state out of the winning region.');
    end
    
    % simulate T time-steps
    y_rand=x0;
    y_wc=x0;
    T=1000; 
    for t=1:T
      % plot the stochastically perturbed trajectory
      x_rand = oscillator(y_rand(end,:),'uniform_stochastic'); 
      y_rand=[y_rand; x_rand(end,:)];
      plot(y_rand(end-1:end,1),y_rand(end-1:end,2),'r.-');
      % plot the worst-case perturbed trajectory
      x_wc = oscillator(y_wc(end,:),'worst_case'); 
      y_wc=[y_wc; x_wc(end,:)];
      plot(y_wc(end-1:end,1),y_wc(end-1:end,2),'k.-');
      pause(0.05)
    end

    savefig('traj_both');

    function xn = oscillator(x,noise_type)
        xn = zeros(size(x));
        a = x;
        switch noise_type
            case 'uniform_stochastic'
                % uniformly distributed noise within the interval [-W_ub,W_ub]
                d = [2*W_ub(1)*rand-W_ub(1), 2*W_ub(2)*rand-W_ub(2)]; % uniform disturbance
            case 'normal_stochastic'
                % truncated to the interval [-W_ub,W_ub]
                d = [max(min(randn,W_ub(1)),-W_ub(1)), max(min(randn,W_ub(2)),-W_ub(2))]; % truncated normal distribution
            case 'worst_case'
                % some handpicked values
                d = [W_ub(1), -W_ub(2)]; % constant disturbance
        end
        xn(1) = a(1) + tau*a(2) + d(1);
        xn(2) = a(2) + tau*(-a(1)+(1-a(1)^2)*a(2)) + d(2);
    end


end



